function createDivs() {
  const container = document.getElementById("bai4");
  for (let i = 1; i < 11; i++) {
    const div = document.createElement("div");
    div.className = "p-2 text-white";
    if (i % 2 == 0) {
      div.innerHTML = `Div chẵn ${i}`;
      div.style.backgroundColor = "red";
    } else {
      div.innerHTML = `Div lẻ ${i}`;
      div.style.backgroundColor = "blue";
    }
    container.appendChild(div);
  }
}

// Hàm createDivs() sử dụng vòng lặp For để tạo ra 10 thẻ div. 
// Khi mỗi thẻ div được tạo ra, chúng ta kiểm tra xem nó có phải là div chẵn hay lẻ bằng cách kiểm tra nếu số thứ tự của div là chẵn hay lẻ. 
// Nếu số thứ tự là chẵn, ta thiết lập background color là màu đỏ, nếu không, background color sẽ là màu xanh. 
// Cuối cùng, thẻ div được thêm vào trong thẻ body của trang web.