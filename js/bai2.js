function calculateSum() {
  const x = parseInt(document.getElementById("xNumber").value*1);
  const n = parseInt(document.getElementById("nNumber").value*1);
  let sum = 0;
  for (let i = 1; i <= n; i++) {
    sum += Math.pow(x, i);
  }
  document.getElementById("bai2").innerHTML = `Tổng S(n) = ${sum}`;
}

// Hàm calculateSum() lấy giá trị của hai trường nhập liệu và sử dụng một vòng lặp for để tính tổng của các số mũ của x từ 1 đến n. 
// Sau đó, tổng được in ra trong thẻ p với id là "bai2" bằng cách sử dụng thuộc tính innerHTML. 
// Hàm Math.pow() được sử dụng để tính số mũ của x.