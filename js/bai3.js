function calculateFactorial() {
  const n = parseInt(document.getElementById("nFactorial").value*1);
  let factorial = 1;
  for (let i = 1; i <= n; i++) {
    factorial *= i;
  }
  document.getElementById(
    "bai3"
  ).innerHTML = `Giai thừa của ${n} là ${factorial}`;
}

// Hàm calculateFactorial() lấy giá trị của trường nhập liệu nFactorial và sử dụng một vòng lặp for để tính giai thừa của n bằng cách nhân các số từ 1 đến n với nhau. 
// Sau đó, kết quả được in ra trong thẻ p với id là "bai3" bằng cách sử dụng thuộc tính innerHTML.