function findSmallestInteger() {
  let sum = 0;
  let n = 1;
  while (sum <= 10000) {
    n++;
    sum += n;
  }
  return n;
}
document.getElementById("bai1").innerHTML = "Số nguyên dương nhỏ nhất sao cho tổng của các số nguyên từ 1 đến n lớn hơn 10000 là: " + findSmallestInteger();

// Chương trình sử dụng một hàm JavaScript có tên findSmallestInteger() để tìm số nguyên dương nhỏ nhất. Hàm này sử dụng một vòng lặp while để tính tổng của các số nguyên từ 1 đến n cho đến khi tổng lớn hơn 10000. Sau đó, n sẽ được trả về.
// Trong phần HTML, có một thẻ p với id là "bai1" để in kết quả của hàm findSmallestInteger().
// Trong phần JavaScript, document.getElementById("`bai1`").innerHTML được sử dụng để thiết lập nội dung của thẻ p để hiển thị kết quả của hàm findSmallestInteger().